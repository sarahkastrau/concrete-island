+++
title = "Datenschutz"
description = "datenschutz"
date = "2019-01-01"
author ="Sarah Kastrau"
+++



<!--more-->

Bei diesem Projekt handelt es sich um meine Bachelor-Arbeit. Somit ist es unkomerziell. Eure Daten werden ausschließlich im Rahmen des Projekts, für den Versand des Newsletters und/oder des Fotobuchs an euch genutzt. Eure Daten werden nicht veröffentlich oder an Dritte weitergegeben. Die Website verwendet keine Cookies.