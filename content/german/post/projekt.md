+++
title = "Das Projekt"
date = "2020-09-14"
description ="Das Projekt"
draft = false
weight = 10
translationKey = "projekt"
tags ="about"
comment = false
prevnext = false
related = false
toc = false
+++

Über mich: Ich bin [Sarah Kastrau](https://sarahkastrau.com) und studiere Fotografie an der Fachhochschule Dortmund. "Concrete Island" ist meine Bachelor-Arbeit.

<!--more-->

"Concrete Island"[^1] ist eine Reihe von interaktiven Ausstellung im öffentlichen Raum an der Schnittstelle von analogen und digitalen Medien. Das Ziel ist, es neue Formen zur Präsentation von Fotografie zu finden.
Fotografischer Inhalt der Arbeit sind die post-industriellen (Stadt)Landschaften des Ruhrgebiets. Im Jahr 2004 lagen über 8.000 Hektar im Ruhrgebiet brach, darunter 460 Gewerbe- und Industriebrachen und 480 Zechenbrachen.
Diese gigantische Fläche lässt sich meiner Meinung nach schwer in der geschlossenen Sauberkeit der klassischen Galerie erfahren. Deswegen will ich die Bilder zurück an ihren Ursprungsort bringen. Die Besucher:innen sollen sich den fotografisch dargestellten Raum selbst zu Fuß erwandern, um so ein Gefühl für die Dimensionen zu bekommen.

Vor meinem Fotografiestudium habe ich Informatik und IT-Security studiert. Dies schlägt sich im technischen Teil der Arbeit nieder. Ich nutze GPS, NFC-Chips, AR, diverse Apps und Open Source Software. Die Prinzipien von D.I.Y. und Open Source sind mir wichtig, deswegen enthält die Website eine Liste aller verwendeten Programme, damit auch andere diese nutzen können. Außerdem gibt es eine kleine Literaturliste mit Texten zum Thema urbaner Raum.

[^1]: Der Titel ist dem gleichnamigen Roman von J.G. Ballard entliehen.