+++
title = "Software"
slug= "software"
date = "2020-11-06"
description ="Software"
draft = false
weight = 70
translationKey = "Software"
tags ="tools"
comment = false
prevnext = false
related = false
toc = false
+++

Eine Sammlung von Software, Tools und Diensten die für das Projekt verwendet wurden. Die Liste wird laufend aktualisiert.

<!--more-->

Die Software & Dienste sind weitestgehend Open Source und/oder kostenlos. Kostenpflichtige Dienste sind mit einem €-Zeichen gekennzeichnet.

**Website**
* Website-Generator: [Hugo](https://gohugo.io/)
* Theme: [SK3](https://themes.gohugo.io/hugo-theme-sk3/)  
* Hosting: [GitLab](https://about.gitlab.com/)  
* Domain: [strato.de](https://www.strato.de) €   

**Apps**
* Augmented Reality: [Artivive](https://artivive.com)
* GPS: [GPS Status And Toolbox](https://play.google.com/store/apps/details?id=com.eclipsim.gpsstatus2&hl) ( nur Android)
* NFC: [NFC Tools](https://www.wakdev.com/en/)
