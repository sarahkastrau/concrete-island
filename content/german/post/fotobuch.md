+++
title = "Das Fotobuch"
date = "2020-09-14"
description ="Das Fotobuch"
draft = false
weight = 30
translationKey = "fotobuch"
tags ="about"
comment = false
prevnext = false
related = false
toc = false
+++

Du hast die Ausstellung besucht? Super. Gib das Passwort ein und du erhälst dein Fotobuch per Post.
<!--more-->
[Hier gehts zum Bestellformular.](http://eepurl.com/gy8uNP)

Du hast die Austellung nicht gefunden? Zu weit? Das Passwort nicht gefunden? Etwas anderes?
Macht ein Foto (oder mehrere) von dem Punkt bis zu dem du gekommen bist (oder benutze eine App um deine GPS-Koordinaten zu bestimmen) und schickt sie mir per Mail an [contact@concrete-island.com](mailto:contact@concrete-island). Du erhältst dein Fotobuch trotzdem.

![Fotobuch01](/zine01.jpg)  
![Fotobuch02](/zine02.jpg)  
![Fotobuch03](/zine03.jpg)  
![Fotobuch04](/zine04.jpg)  