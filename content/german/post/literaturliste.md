+++
title = "Bücher"
date = "2020-09-21"
description ="Bücher"
draft = false
weight = 70
translationKey = "Literaturliste"
tags ="tools"
comment = false
prevnext = false
related = false
toc = false
+++

Eine Sammlung von Texten, Fotobüchern, Filmen und Websites die in die Arbeit eingeflossen sind. Das Jahr bezieht sich auf die jeweilige Erstauflage. Die Liste wird laufend aktualisiert.

<!--more-->
**Bücher**
* [J.G. Ballard - Concrete Island](https://archive.org/details/concreteisland00ball)  
Jahr: 1974, Verlag: Jonathan Cape
* [Nick Dunn - Dark Matters: A Manifesto For The Nocturnal City](https://www.johnhuntpublishing.com/zer0-books/our-books/dark-matters)  
Jahr: 2016, ISBN: 9781782797487, Verlag: Zer0 Books
*  [Marc Augé - Nicht-Orte](https://www.chbeck.de/aug%C3%A9-nicht-orte/product/31863)  
Jahr: 1992, ISBN: 9783406670367, Verlag: C.H. Beck  
* [Mark Fisher - The Weird And The Eerie](https://repeaterbooks.com/product/the-weird-and-the-eerie/)   
Jahr: 2016, ISBN: 1910924385, Verlag Repeater Books   
* [Brian O Doherty - Inside the White Cube: The Ideology of the Gallery Space](https://arts.berkeley.edu/wp-content/uploads/2016/01/arc-of-life-ODoherty_Brian_Inside_the_White_Cube_The_Ideology_of_the_Gallery_Space.pdf)  
Jahr: 1976, ISBN: 0932499058, The Lapis Press

**Fotobücher**  
* [Chargesheimer: Die Entdeckung des Ruhrgebiets](https://www.buchhandlung-walther-koenig.de/koenig2/index.php?mode=details&showcase=1&art=1510967)  
Jahr: 2015, ISBN: 9783863355265, Verlag: Walther König  
* [Richard Misrach / Kate Orff - Petrochemical America](https://aperture.org/never-on-sale/petrochemical-america-signed-edition/)  
Jahr: 2014, ISBN: 9781597111911, Verlag: Aperture  
* [Give Up - Lonely Days and Wasted Nights](http://ingivingup.blogspot.com/2011/03/lonely-days-wasted-nights-2nd-edition.html)  
Jahr: 2011, Verlag: self-published    
* [Santiago Sierra - Haus im Schlamm](https://www.hatjecantz.de/santiago-sierra-1542-1.html)  
Jahr: 2005, ISBN: 9783775715867, Verlag: Hatje Cantz

**Websites**
* [Ninjalicious - No Disclaimer](http://www.infiltration.org/ethics-nodisclaimer.html)
