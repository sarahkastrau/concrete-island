+++
title = "51.515720, 7.432150"
date = "2020-10-03"
description ="Status: aktiv"
draft = true
weight = 40
color ="#ff6e27"
tags = "location"
comment = false
prevnext = false
related = false
toc = false
+++

Status: aktiv

Grund: am 05.10.2020 eröffnet

Koordinaten: <a href="https://goo.gl/maps/DzLjMxxKnEH5iMNu6" style="color:#ff6e27">51.515720, 7.432150</a>

<!--more-->

Wegbeschreibung: von Dortmund Hbf mit der U-Bahn (U47/U45/U41) bis Haltestelle Kampstraße. 
Von Kampstraße mit der U-Bahn (U43/U44) bis Haltestelle Ofenstraße. Ab Ofenstraße circa 15 Minuten Fußweg.

Hinweise: Der Weg führt größtenteils über Asphalt, nur die letzten Meter geht es durchs Gestrüpp. Es wird feste Kleidung empfohlen um Brombeere & Co. abzuwehren. 
Haltet nach Aufklebern Ausschau.

Tools: Mit <a href="https://play.google.com/store/apps/details?id=com.eclipsim.gpsstatus2&hl" style="color:#ff6e27">GPS Status And Toolbox</a> (Android) könnt ihr euren Standort bestimmen.  
Unter <a href="https://luftbilder.geoportal.ruhr/" style="color:#ff6e27">luftbilder.geoportal.ruhr/</a> findet ihr aktuelle und historische Luftaufnahmen des Ruhrgebiets.