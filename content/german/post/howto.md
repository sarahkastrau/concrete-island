+++
title = "Wie es funktioniert"
date = "2020-09-14"
description ="Wie es funktioniert"
draft = false
weight = 20
translationKey = "howto"
tags ="about"
comment = false
prevnext = false
related = false
toc = false
+++

Jede Ausstellung findet in einer anderen Stadt statt. Jede Stadt erfordert die Suche nach einem neuen Ort.

<!--more-->
Diese Erkundungen werden fotografisch dokumentiert und in einem Fotobuch zusammengefasst das die Austellung ergänzt. So wird jedes Buch etwas anders, eine sich stets verändernde Karte des urbanen Raums. Das Buch kann nicht gekauft werden sondern ist nur im Tausch gegen ein Passwort erhältlich das am Austellungsort versteckt ist. Der Ort der Ausstellung wird über die Website, [Instagram](https://instagram.com/sarahkastrau) und den [Newsletter](http://eepurl.com/gy8uNP) bekannt gegeben. Wenn ihr das Passwort gefunden habt geht auf [concrete-island.com/post/fotobuch](/post/fotobuch) und gebt es zusammen mit eurer Anschrift[^1] ein. Ihr erhaltet euer Fotobuch per Post. Der Versand ist kostenlos.
![Flowchart](/flowchart-de.png)

**Stolperfallen**  
Ihr habt die Ausstellung oder das Passwort nicht gefunden? Zu weit? Etwas anderes?  
Macht ein Foto von dem Punkt bis zu dem ihr gekommen seid (GPS-Koordinaten sind auch super) und schickt es mir per [Mail](mailto:contact@concrete-island.com). Ihr erhaltet euer Fotobuch trotzdem.

**Wichtig**  
Das Ganze soll keine Mutprobe sein. Einige Orte sind recht abgelegen, andere schwer zugänglich. Solltet ihr euch aus irgendeinem Grund bei eurer Erkundung unwohl fühlen, dann brecht ab und macht euch auf dem Heimweg. Mir ist es wichtig, dass bei diesem Projekt der Spaß an der Erkundung des urbanen Raums im Vordergrund steht. Damit möglichst viele Menschen Spaß an der Sache haben, bin ich auf euer Feedback angewiesen. Schreibt mir gerne per Mail oder bei Instagram, was ich verbessern kann.


[^1]: Datenschutz: eure Daten werden ausschließlich im Rahmen des Projekts genutzt, nicht veröffentlich oder an andere weitergegeben.