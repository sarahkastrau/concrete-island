+++
title = "How it works"
date = "2020-09-14"
description ="How it works"
draft = false
weight = 20
translationKey = "howto"
+++

Every exhibition takes place in a different city. Every city requires the search for a new location.

<!--more-->

These explorations are documented and become a part of the photobook which accompanies the exhibition. This way each book will be different, an ever changing map of the urban space. The books is not for sale and can only be obtained in exchange for a password hidden at the location. The locations are announced on this website, [Instagram](https://instagram.com/sarahkastrau) and my [Newsletter](http://eepurl.com/gy8uNP). Once you found the password, got to [concrete-island.com/en/post/photobook](/en/post/photobook) and enter it together with your shipping address[^1]. You'll receive your photobook by mail. Shipping is free of charge, worldwide.
![Flowchart](/flowchart-en.png)

**Tripping Hazards**  
You could not find the exhibition or the passwort? Too far? Something else got in the way?
Take a photo (or several) from the position to where you got (or grab the GPS coordinates with an app) and send them to [contact@concrete-island.com](mailto:contact@concrete-island). You'll receive your photobook all the same.

**Important**  
This project is not intended to be a test of courage. Some places are rather remote, others difficult to access and everyone has a different concept of what s/he perceives as save. In case you feel uncomfortable for whatever reason, feel free to go home. The most important part for me is that you enjoy your exploration. In order to make this project fun for as many people as possible, I need your feedback. Feel free to message me by mail or in Instagram and let me know what I can improve.


[^1]: Privacy: your information will only be used within the context of this project and won't be published or shared with third parties. This website does not use cookies.