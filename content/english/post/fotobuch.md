+++
title = "The Photobook"
slug = "photobook"
date = "2020-09-14"
description ="The Photobook"
draft = false
weight = 30
translationKey = "fotobuch"
+++

You visited the exhibition? Great. Enter the password and your shipping address to receive your photobook by mail. 

<!--more-->

[Click here for the order form.](http://eepurl.com/gy8uNP)

You could not find the exhibition? Too far? Password gone? Something else got in the way?
Take a photo (or several) from the position to where you got (or grab the GPS coordinates with an app) and send them to [contact@concrete-island.com](mailto:contact@concrete-island). You'll receive your photobook all the same.

![Fotobuch01](/zine01.jpg)  
![Fotobuch02](/zine02.jpg)  
![Fotobuch03](/zine03.jpg)  
![Fotobuch04](/zine04.jpg)  