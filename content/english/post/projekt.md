+++
title = "The Project"
slug = "project"
date = "2020-09-14"
description ="The Project"
draft = false
weight = 10
translationKey = "projekt"
+++

About myself: I am [Sarah Kastrau](https://sarahkastrau.com) and study photography at the Fachhochschule Dortmund. "Concrete Island" ist my Bachelor project.

<!--more-->

"Concrete Island"[^1] is an interactive exhibition in public space at the intersection of analog and digital media. My aim is to find new forms for the presentation of photography.
The topic of my work are the postindustrial landscapes of the Ruhr Area, a densely populated region known for its extensive coal mining. In 2004 more than 8.000 hectares of former industrial land were laid waste, among them 460 indistrial sites and 480 coalmines.
In my opinion it's impossible to represent this enormous area inside the closed cleanliness of the classic white cube gallery. That's why I'm bringing the photos back to where they came from. Visitors will have to conquer the photographed space by foot, in order to gain an understanding of its dimensions.

Prior to photography I studied Computer Science and IT-Security, which influenced the technical aspect of the project. I'm going to use GPS, NFC tags, augmented reality, a variety of apps and open source software. The principles of d.i.y. and open source are important to me. This is why I'll include a list of the programs used so others may find something useful for their own work. There is also a growing list of books on the topic of urban space.  

[^1]: The title is taken from the novel by J.G. Ballard.