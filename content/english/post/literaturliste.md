+++
title = "Books"
slug = "books"
date = "2020-09-21"
description ="Books"
draft = false
weight = 70
translationKey = "Literaturliste"
tags ="tools"
comment = false
prevnext = false
related = false
toc = false
+++

A collection of texts, photobooks, movies and websites that fueled the creation of this project. The year refers to the respective first edition. The list will be updated continously.

<!--more-->
**Books**
* [J.G. Ballard - Concrete Island](https://archive.org/details/concreteisland00ball)  
Year: 1974, Publisher: Jonathan Cape
* [Nick Dunn - Dark Matters: A Manifesto For The Nocturnal City](https://www.johnhuntpublishing.com/zer0-books/our-books/dark-matters)  
Year: 2016, ISBN: 9781782797487, Publisher: Zer0 Books
*  [Marc Augé - Nicht-Orte](https://www.chbeck.de/aug%C3%A9-nicht-orte/product/31863)  
Year: 1992, ISBN: 9783406670367, Publisher: C.H. Beck  
* [Mark Fisher - The Weird And The Eerie](https://repeaterbooks.com/product/the-weird-and-the-eerie/)   
Year: 2016, ISBN: 1910924385, Publisher Repeater Books   
* [Brian O Doherty - Inside the White Cube: The Ideology of the Gallery Space](https://arts.berkeley.edu/wp-content/uploads/2016/01/arc-of-life-ODoherty_Brian_Inside_the_White_Cube_The_Ideology_of_the_Gallery_Space.pdf)  
Year: 1976, ISBN: 0932499058, The Lapis Press

**Photobooks**  
* [Chargesheimer: Die Entdeckung des Ruhrgebiets](https://www.buchhandlung-walther-koenig.de/koenig2/index.php?mode=details&showcase=1&art=1510967)  
Year: 2015, ISBN: 9783863355265, Publisher: Walther König  
* [Richard Misrach / Kate Orff - Petrochemical America](https://aperture.org/never-on-sale/petrochemical-america-signed-edition/)  
Year: 2014, ISBN: 9781597111911, Publisher: Aperture  
* [Give Up - Lonely Days and Wasted Nights](http://ingivingup.blogspot.com/2011/03/lonely-days-wasted-nights-2nd-edition.html)  
Year: 2011, Publisher: self-published    
* [Santiago Sierra - Haus im Schlamm](https://www.hatjecantz.de/santiago-sierra-1542-1.html)  
Year: 2005, ISBN: 9783775715867, Publisher: Hatje Cantz

**Websites**
* [Ninjalicious - No Disclaimer](http://www.infiltration.org/ethics-nodisclaimer.html)
