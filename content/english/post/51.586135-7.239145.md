+++
title = "51.586135, 7.239145"
date = "2020-10-16"
description ="Status: aktiv"
draft = true
weight = 40
translationKey = "Orthlohstrasse"
color ="#ff6e27"
tags = "location"
comment = false
prevnext = false
related = false
toc = false
+++

Status: active

Reason: opened on 16.10.2020

Coordinates: <a href="https://goo.gl/maps/Xxt1jp5ZAwYiuYDt5" style="color:#ff6e27">51.586135, 7.239145</a>

<!--more-->

Directions:
From Recklinghausen Hbf: take the bus (line 236 or 237) to stop Lansingfeld. 
From Herne Hbf: take the bus SB20 to stop König-Ludwig-Str., fro there take the bus 210 to stop Lansingfeld.
From Lansingfeld it is a five minute walk. I left stickers along the way for better orientation.

Notes: The route is all tarmac, the location itself is unmade. Sturdy and water resistant shoes are recommended. A part of the route goes along the bicycle path König-Ludwig-Trasse, so watch out for cyclist. If you want to travel by bike yourself, follow the path past the entrance for about a hundred meters. There is a bicycle stand on the left side.

Tools: You can locate your position with <a href="https://play.google.com/store/apps/details?id=com.eclipsim.gpsstatus2&hl" style="color:#ff6e27">GPS Status And Toolbox</a> (Android). <a href="https://luftbilder.geoportal.ruhr/" style="color:#ff6e27">luftbilder.geoportal.ruhr/</a> offers and extensive collection of current and historic aerial views of the Ruhr Area.

Various: If you take photos yourself I'd love to see them and, with your permission, include them into the documentation of the project.