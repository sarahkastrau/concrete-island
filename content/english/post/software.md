+++
title = "Software"
date = "2020-11-06"
slug ="software"
description ="Software"
draft = false
weight = 70
translationKey = "Software"
tags ="tools"
comment = false
prevnext = false
related = false
toc = false
+++

A collection of software, tools and services used for this project. The list will be updated continously.

<!--more-->

Software and services are mostly open source and/or for free. Paid services are marked with a €-sign.

**Website**
* Website-Generator: [Hugo](https://gohugo.io/)
* Theme: [SK3](https://themes.gohugo.io/hugo-theme-sk3/)  
* Hosting: [GitLab](https://about.gitlab.com/)  
* Domain: [strato.de](https://www.strato.de) €   

**Apps**
* Augmented Reality: [Artivive](https://artivive.com)
* GPS: [GPS Status And Toolbox](https://play.google.com/store/apps/details?id=com.eclipsim.gpsstatus2&hl) (Android only)
* NFC: [NFC Tools](https://www.wakdev.com/en/)
